console.log(document);
// console.log(document.querySelector("#txt-first-name"));
// result - element from html
/*
	document - refers to the whole webpage
	querySelector - used to select a specific element (object) as long as it is inside the html tag(HTML ELEMENT)
*/

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullNameA = document.querySelector('#span-full-name-A');
const spanFullNameB = document.querySelector('#span-full-name-B');

/*
	addEventListener - funtion that lets the webpage to listen to the event performed by the user
	-- takes 2 arguments
	string - the event to which the HTML element will listen, these are predertermined
*/

txtFirstName.addEventListener('keyup',(event)=>{
	spanFullNameA.innerHTML = event.target.value
})

txtLastName.addEventListener('keyup',(event)=>{
	spanFullNameB.innerHTML = event.target.value
})

function firstName(){
	spanFullNameA.innerHTML = event.target.value;
}

function lastName(){
	spanFullNameB.innerHTML = event.target.value;
}
