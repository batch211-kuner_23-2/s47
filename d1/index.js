console.log(document);
console.log(document.querySelector("#txt-first-name"));
// result - element from html
/*
	document - refers to the whole webpage
	querySelector - used to select a specific element (object) as long as it is inside the html tag(HTML ELEMENT)
*/

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

/*
	addEventListener - funtion that lets the webpage to listen to the event performed by the user
	-- takes 2 arguments
	string - the event to which the HTML element will listen, these are predertermined
*/

txtFirstName.addEventListener('keyup',(event)=>{
	spanFullName.innerHTML = txtFirstName.value
})

txtFirstName.addEventListener('keyup',(event)=>{
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
})

// Mini Activity1

txtLastName.addEventListener('keyup',(event)=>{
	spanFullName.innerHTML = txtLastName.value
})

txtLastName.addEventListener('keyup',(event)=>{
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
})


// Activity s47
spanFullName.addEventListener('load',(event)=>{
	spanFullName.innerHTML = spanFullName.value
})

spanFullName.addEventListener('load',(event)=>{
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
})